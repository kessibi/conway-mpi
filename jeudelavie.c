// Jeu de la vie avec sauvegarde de quelques itérations
// compiler avec gcc -O3 -march=native (et -fopenmp si OpenMP souhaité)

#include <mpi.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

// hauteur et largeur de la matrice
#define HM 1200
#define LM 800

// nombre total d'itérations
#define ITER 10001
// multiple d'itérations à sauvegarder
#define SAUV 1000

#define DIFFTEMPS(a, b) \
  (((b).tv_sec - (a).tv_sec) + ((b).tv_usec - (a).tv_usec) / 1000000.)

/* tableau de cellules */
typedef char Tab[HM][LM];

// initialisation du tableau de cellules
void init(Tab);

// calcule une nouveau tableau de cellules à partir de l'ancien
// - paramètres : ancien, nouveau
void calcnouv(Tab, Tab);

// variables globales : pas de débordement de pile
Tab t1, t2, t3;
Tab tsauvegarde[1 + ITER / SAUV];
int rank, size, off1 = 1, off2 = 0, s;
MPI_Request r1, r2, r3, r4;
MPI_Status s1, s2, s3, s4;

int main(int argc, char **argv) {
  struct timeval tv_init, tv_beg, tv_end, tv_save;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Barrier(MPI_COMM_WORLD);
  if (size == 1) {
    fprintf(stderr, "err: nb. de processus < 2");
    MPI_Finalize();
    exit(1);
  }
  if (HM % size != 0) {
    if (rank == 0) {
      fprintf(stderr, "err: nombre de processus ne divise pas HM (%d)\n", HM);
    }
    MPI_Finalize();
    exit(1);
  }

  if (rank == 0) {
    gettimeofday(&tv_init, NULL);
    init(t1);
    gettimeofday(&tv_beg, NULL);
    off1 = 0;
  }
  if (rank == size - 1) off2 = 1;

  s = HM / size;

  // scatter the 2d array to all ps in MPI_COMM_WORLD
  MPI_Scatter(t1, HM * LM / size, MPI_CHAR, *(t2 + 1), HM * LM / size, MPI_CHAR,
              0, MPI_COMM_WORLD);

  for (int i = 0; i < ITER; i++) {
    if (i % 2 == 0)
      calcnouv(t2, t1);
    else
      calcnouv(t1, t2);

    if (i % SAUV == 0) {
      // gather all info from ps
      MPI_Gather(*(t2 + 1), HM / size * LM, MPI_CHAR, t3, HM / size * LM,
                 MPI_CHAR, 0, MPI_COMM_WORLD);
      if (rank == 0) {
        printf("sauvegarde (%d)\n", i);
        // copying t1 in tsauvegarde[i/SAUV]
        for (int x = 0; x < HM; x++)
          for (int y = 0; y < LM; y++) tsauvegarde[i / SAUV][x][y] = t3[x][y];
      }
    }
  }

  if (rank == 0) {
    gettimeofday(&tv_end, NULL);

    FILE *f = fopen("jdlv.out", "w");
    for (int i = 0; i < ITER; i += SAUV) {
      fprintf(f, "------------------ sauvegarde %d ------------------\n", i);
      for (int x = 0; x < HM; x++) {
        for (int y = 0; y < LM; y++)
          fprintf(f, tsauvegarde[i / SAUV][x][y] ? "*" : " ");
        fprintf(f, "\n");
      }
    }
    fclose(f);

    gettimeofday(&tv_save, NULL);
  }

  if (rank == 0) {
    printf("init : %lf s,", DIFFTEMPS(tv_init, tv_beg));
    printf(" calcul : %lf s,", DIFFTEMPS(tv_beg, tv_end));
    printf(" sauvegarde : %lf s\n", DIFFTEMPS(tv_end, tv_save));
  }
  MPI_Finalize();
  return (0);
}

void init(Tab t) {
  srand(time(0));
  for (int i = 0; i < HM; i++)
    for (int j = 0; j < LM; j++) {
      // t[i][j] = rand()%2;
      // t[i][j] = ((i+j)%3==0)?1:0;
      // t[i][j] = (i==0||j==0||i==h-1||j==l-1)?0:1;
      t[i][j] = 0;
    }
  t[10][10] = 1;
  t[10][11] = 1;
  t[10][12] = 1;
  t[9][12] = 1;
  t[8][11] = 1;

  t[55][50] = 1;
  t[54][51] = 1;
  t[54][52] = 1;
  t[55][53] = 1;
  t[56][50] = 1;
  t[56][51] = 1;
  t[56][52] = 1;
}

int nbvois(Tab t, int i, int j) {
  int n = 0;
  if (i > 0) { /* i-1 */
    if (j > 0)
      if (t[i - 1][j - 1]) n++;
    if (t[i - 1][j]) n++;
    if (j < LM - 1)
      if (t[i - 1][j + 1]) n++;
  }
  if (j > 0)
    if (t[i][j - 1]) n++;
  if (j < LM - 1)
    if (t[i][j + 1]) n++;
  if (i < HM / size + 1) { /* i+1 */
    if (j > 0)
      if (t[i + 1][j - 1]) n++;
    if (t[i + 1][j]) n++;
    if (j < LM - 1)
      if (t[i + 1][j + 1]) n++;
  }
  return (n);
}

void calcnouv(Tab t, Tab n) {
  int v;

  if (rank == 0) {
    MPI_Irecv(*(t + HM / size + 1), LM, MPI_CHAR, 1, 0, MPI_COMM_WORLD, &r1);
    MPI_Isend(*(t + (HM / size)), LM, MPI_CHAR, 1, 0, MPI_COMM_WORLD, &r2);
  } else if (rank == size - 1) {
    MPI_Irecv(t, LM, MPI_CHAR, size - 2, 0, MPI_COMM_WORLD, &r1);
    MPI_Isend(*(t + 1), LM, MPI_CHAR, size - 2, 0, MPI_COMM_WORLD, &r2);
  } else {  // middle ranks
    MPI_Irecv(*(t + HM / size + 1), LM, MPI_CHAR, rank + 1, 0, MPI_COMM_WORLD,
              &r1);
    MPI_Irecv(t, LM, MPI_CHAR, rank - 1, 0, MPI_COMM_WORLD, &r2);
    MPI_Isend(*(t + 1), LM, MPI_CHAR, rank - 1, 0, MPI_COMM_WORLD, &r3);
    MPI_Isend(*(t + HM / size), LM, MPI_CHAR, rank + 1, 0, MPI_COMM_WORLD, &r4);
  }

  // computing middle cells, the ones that dont need exchanges
  for (int i = 1 + off1; i < HM / size + off2; i++) {
    for (int j = 0; j < LM; j++) {
      v = nbvois(t, i, j);
      if (v == 3)
        n[i][j] = 1;
      else if (v == 2)
        n[i][j] = t[i][j];
      else
        n[i][j] = 0;
    }
  }

  // waiting for request r1 to finish, to compute another line
  MPI_Wait(&r1, &s1);
  if (rank > 0 && rank < size - 1) {
    // same but for r2, to compute one more line
    // used for every ps with rank not 0 or size - 1
    MPI_Wait(&r2, &s2);
  } 

  // computing very last line
  if (rank < size - 1) {
    for (int j = 0; j < LM; j++) {
      v = nbvois(t, s, j);
      if (v == 3)
        n[s][j] = 1;
      else if (v == 2)
        n[s][j] = t[s][j];
      else
        n[s][j] = 0;
    }
  }

  // computing very first line
  if (rank > 0) {
    for (int j = 0; j < LM; j++) {
      v = nbvois(t, 1, j);
      if (v == 3)
        n[1][j] = 1;
      else if (v == 2)
        n[1][j] = t[1][j];
      else
        n[1][j] = 0;
    }
  }
  
  // wait for all requests to properly stop
  if (rank == 0 || rank == size - 1) {
    MPI_Wait(&r2, &s2);
    MPI_Wait(&r2, &s2);
  } else {  // middle ranks
    MPI_Wait(&r3, &s3);
    MPI_Wait(&r4, &s4);
  }
}
