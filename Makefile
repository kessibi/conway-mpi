CC = mpicc
CFLAGS = -Wall -Wextra -Werror -O3 -march=native
BIN = a.out
SEQ_BIN = seq.out
OS := $(shell uname -s)
HOSTFILE = hostfile.txt
TUNEFILE = tune.txt

MPI_CONFIG =

ifneq ("$(wildcard $(HOSTFILE))", "")
	MPI_CONFIG += -hostfile $(HOSTFILE)
endif

ifneq ("$(wildcard $(TUNEFILE))", "")
	MPI_CONFIG += -tune $(TUNEFILE)
endif

ifeq ($(OS),Darwin)
	SEQ_CC := $(if $(shell which gcc-8), gcc-8, clang)
else
	SEQ_CC = gcc
endif

ifneq ($(n),)
	MPI_CONFIG += -n $(n)
endif

.PHONY: all clean compare

mpi: jeudelavie.c
	$(CC) $^ $(CFLAGS)

seq: seq_jeudelavie.c
	$(SEQ_CC) $^ $(CFLAGS) -fopenmp -o $(SEQ_BIN)

all: mpi seq

run_all: run run_seq

run: mpi
	mpirun $(MPI_CONFIG) $(BIN) 

run_seq: seq
	./$(SEQ_BIN)

compare: run run_seq
	cmp jdlv.out seq_jdlv.out

clean:
	rm *.out

